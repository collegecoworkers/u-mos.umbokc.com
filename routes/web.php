<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'SiteController@Index');
Route::get('/view/{id}', 'SiteController@View');
Route::get('/edit/{id}', 'SiteController@Edit');
Route::get('/delete/{id}', 'SiteController@Delete');

Route::post('/add', 'SiteController@Add');
Route::post('/update/{id}', 'SiteController@Update');

Route::get('/edit-user/{id}', 'AdminController@EditUser');
Route::get('/delete-user/{id}', 'AdminController@DeleteUser');
Route::post('/update-user/{id}', 'AdminController@UpdateUser');

Route::get('/add-cat', 'AdminController@AddCat');
Route::get('/edit-cat/{id}', 'AdminController@EditCat');
Route::get('/delete-cat/{id}', 'AdminController@DeleteCat');
Route::post('/create-cat', 'AdminController@CreateCat');
Route::post('/update-cat/{id}', 'AdminController@UpdateCat');

