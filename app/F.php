<?php
namespace App;

class F {
  public static function toArr($items, $val = 'title', $key = 'id'){
    $items_arr = [];
    foreach ($items as $item){
      if(is_callable($val)){
        $items_arr[$item->{$key}] = $val($item);
      } else {
        $items_arr[$item->{$key}] = $item->{$val};
      }
    }
    return $items_arr;
  }
}
