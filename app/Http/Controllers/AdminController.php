<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	Cat,
	Card,
	User
};

class AdminController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function EditUser($id) {
		$user = User::where('id', $id)->first();
		return view('admin/edit-user')->with([
			'model' => $user,
		]);
	}
	public function UpdateUser($id, Request $request) {
		$model = User::where('id', $id)->first();

		$model->full_name = request()->full_name;
		$model->name = request()->name;
		$model->email = request()->email;
		$model->is_admin = request()->is_admin;

		$model->save();
		return redirect()->to('/');
	}
	public function DeleteUser($id) {
		User::where('id', $id)->delete();
		return redirect()->to('/');
	}

	public function CreateCat(Request $request) {
		$model = new Cat();

		$model->title = request()->title;
		$model->desc = request()->desc == '' ? ' ' : request()->desc;

		$model->save();
		return redirect('/');
	}
	public function EditCat($id) {
		$cat = Cat::where('id', $id)->first();
		return view('admin.edit-cat')->with([
			'model' => $cat,
		]);
	}
	public function UpdateCat($id, Request $request) {
		$model = Cat::where('id', $id)->first();

		$model->title = request()->title;
		$model->desc = request()->desc == '' ? ' ' : request()->desc;

		$model->save();
		return redirect()->to('/');
	}
	public function DeleteCat($id) {
		Cat::where('id', $id)->delete();
		return redirect()->to('/');
	}
}
