<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	Cat,
	Dok,
	User
};

class SiteController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Index() {
		$is_admin = User::isAdmin();
		// $is_admin = true;

		$doks = Dok::where('user_id', auth()->user()->id)->get();
		$cats_arr = Cat::allArr();

		if ($is_admin) {
			$users = User::all();
			$cats = Cat::all();
			$all_doks = Dok::all();
			// dbg($all_doks);
		}
		return view('index')->with([
			'cats_arr' => $cats_arr,
			'doks' => $doks,
			'user' => User::curr(),
			'cats' => $is_admin ? $cats : null,
			'users' => $is_admin ? $users : null,
			'all_doks' => $is_admin ? $all_doks : null,
			'is_admin' => $is_admin,
		]);
	}

	public function View($id) {
		$dok = Dok::where('id', $id)->first();
		$cat = Cat::where('id', $dok->cat_id)->first();
		return view('view')->with([
			'cat' => $cat,
			'item' => $dok,
		]);
	}

	public function Edit($id) {
		$dok = Dok::where('id', $id)->first();
		$cats = Cat::all();
		$cats_arr = [];
		foreach ($cats as $item){
			$cats_arr[$item->id] = $item->title;
		}
		return view('edit')->with([
			'cats_arr' => $cats_arr,
			'model' => $dok,
		]);
	}

	public function Add(Request $request) {

		$model = new Dok();

		$model->title = request()->title;
		$model->cat_id = request()->cat_id;
		$model->desc = request()->desc;
		$model->props = request()->props;
		$model->user_id = auth()->user()->id;

		$model->save();

		return redirect('/');
	}

	public function Update($id, Request $request) {
		$model = Dok::where('id', $id)->first();

		$model->title = request()->title;
		$model->cat_id = request()->cat_id;
		$model->desc = request()->desc;
		$model->props = request()->props;

		$model->save();
		return redirect()->to('/');
	}

	public function Delete($id) {
		Dok::where('id', $id)->delete();
		return redirect()->to('/');
	}
}
