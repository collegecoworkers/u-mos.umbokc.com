require('./bootstrap');
window.ProgressBar = require('progressbar.js')
require('rangeslider.js')

window.App = {
	textContent: ('textContent' in document) ? 'textContent' : 'innerText',
	get_from: document,
	$id: function(id){
		var val = this.get_from.getElementById(id);
		this.reset_get_from();
		return val;
	},
	$name: function(name){
		var val = this.get_from.getElementsByName(name);
		this.reset_get_from();
		return val;
	},
	$get: function(sel){
		return this.$gets(sel)[0];
	},
	get: function(sel){
		return this.$get(sel);
	},
	$gets: function(sel){
		var val = this.get_from.querySelectorAll(sel);
		this.reset_get_from();
		return val;
	},
	gets: function(sel){
		return this.$gets(sel);
	},
	$get_filter: function(f){
		return this.$get('.filters [filter-id='+f+']');
	},
	$get_text: function(t){
		return this.$id(t);
	},
	reset_get_from: function(){
		this.get_from = document;
	},

	$ajax: function (data) {
		$.ajax({
			url: location.origin + '/' + data.location,
			method: "POST",
			data: data.data,
			dataType: "json",
			success: data.success
		}).fail(data.fail);
	},

	dbg: function(mes){
		window.console.log(mes);
		window.dbg = mes;
	},
};

if(window.location.pathname == '/'){

	var progress_items = $('.progress-item');

	var get_options_prog_bar = function(icon, value, text){
		var num = Math.ceil((value)*100);
		text = (text != null) ? text.replace('{num}', num) : num;
		return {
			strokeWidth: 10,
			duration: 1000,
			text:{
				value: '<i class="icon fa fa-'+icon+'"></i><br><span class="text">'+text+'</span>',
				className: 'progressbar__label',
			}
		};
	}

	var set_value_progress = function(item, value, text){
		var num = Math.ceil((value)*100);
		var text = (text != null) ? text.replace('{num}', num) : num;
		item.set(value);
		App.get_from = item.text;
		App.$get('.text').innerText = text;
	}

	var set_value_to_db = function(name, value){
		App.$ajax({
			location: 'counter',
			data: { _token: $('meta[name="csrf-token"]').attr('content'), name: name, value: value },
			success: function (msgs) {
				App.dbg(msgs);
			},
			fail: function (jqXHR, textStatus) {
				App.dbg("Request failed: " + textStatus + ' : ' + jqXHR.responseText);
			}
		});
	}

	App.items_progress = {};

	progress_items.each(function(index, el) {
		var the = $(el);

		var item = { value: the.attr('value'), icon: the.attr('icon')};
		var _item = the.attr('counter');

		if(the.attr('block')) item['block'] = (the.attr('block') == 'true');
		if(the.attr('text')) item['text'] = the.attr('text');

		var text = item.hasOwnProperty('text') ? item.text : null;
		var val_item = item.value;
		var wrap = '.progress-item.'+_item;

		App.items_progress[_item] = new ProgressBar.Circle((wrap + ' .progress'), get_options_prog_bar(item.icon, val_item, text));

		if (!item.hasOwnProperty('block') || item.block === false){
			$(wrap + ' input[type="range"]').val(val_item).change();

			$(wrap + ' input[type="range"]').rangeslider({
				polyfill: false,
				wrap_item: _item,
				text_item: text,
				onInit: function() {
				},
				onSlide: function(position, value) {
					set_value_progress(App.items_progress[this.options.wrap_item], value, this.options.text_item);
				},
				onSlideEnd: function(position, value) {
					set_value_to_db(this.options.wrap_item, value);
				}
			});
		}

		App.items_progress[_item].set(val_item);
	});

} else if (window.location.pathname == '/devices'){
	$('.device-item .panel button').click(function(event) {
		var the = $(this);
		var panel = the.parent();
		var status = panel.find('.status');
		var value = 0;
		if(status.text() == 'off'){
			status.text('on');
			status.addClass('on');
			value = 1;
		} else {
			status.text('off');
			status.removeClass('on');
			value = 0;
		}
		App.$ajax({
			location: 'device',
			data: { _token: $('meta[name="csrf-token"]').attr('content'), name: panel.attr('device'), value: value },
			success: function (msgs) {
				App.dbg(msgs);
			},
			fail: function (jqXHR, textStatus) {
				App.dbg("Request failed: " + textStatus + ' : ' + jqXHR.responseText);
			}
		});
	});
} else if (window.location.pathname == '/tv'){
	$('.tv-items button').click(function(event) {
		var the = $(this);
		if(!the.hasClass('active')){
			$('.tv-items button').removeClass('active');
			the.addClass('active');
			App.$ajax({
				location: 'tv',
				data: { _token: $('meta[name="csrf-token"]').attr('content'), name: the.attr('chanel') },
				success: function (msgs) {
					App.dbg(msgs);
				},
				fail: function (jqXHR, textStatus) {
					App.dbg("Request failed: " + textStatus + ' : ' + jqXHR.responseText);
				}
			});
		}
	});
}
