@extends('layout.app')
@section('content')

<nav id="nav">
  <a href="/" class="icon fa-arrow-left active"><span>Вернуться</span></a>
</nav>

<div id="main">
  <article id="add-cat" class="panel">
    <header>
      <h2>Изменить пользователя</h2>
    </header>
    {!! Form::open(array('url' => '/update-user/' .$model->id )) !!}
    <div>
      <div class="row">
        <div class="12u$">
          {!! Form::text('full_name', $model->full_name, ['placeholder' => 'Имя',]) !!}
          @if ($errors->has('full_name')) <span class="help-block"><strong>{{ $errors->first('full_name') }}</strong></span> @endif
        </div>
        <div class="12u$">
          {!! Form::text('name', $model->name, ['placeholder' => 'Логин',]) !!}
          @if ($errors->has('name')) <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span> @endif
        </div>
        <div class="12u$">
          {!! Form::text('email', $model->email, ['placeholder' => 'Почта',]) !!}
          @if ($errors->has('email')) <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span> @endif
        </div>
        <div class="12u$">
          <label>Тип</label>
          {!! Form::select('is_admin', ['0' => 'Пользователь', '1' => 'Админ'], $model->is_admin) !!}
          @if ($errors->has('is_admin')) <span class="help-block"><strong>{{ $errors->first('is_admin') }}</strong></span> @endif
        </div>
        <div class="12u$">
          <input type="submit" value="Сохранить" />
        </div>
      </div>
    </div>
    {!! Form::close() !!}
  </article>
</div>

@endsection
