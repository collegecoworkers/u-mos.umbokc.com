@extends('layout.app')
@section('content')

<nav id="nav">
	<a href="/" class="icon fa-arrow-left active"><span>Вернуться</span></a>
</nav>

<div id="main">
	<article id="add-cat" class="panel">
		<header>
			<h2>Изменить категорию</h2>
		</header>
		@include('parts._form_cat')
	</article>
</div>

@endsection
