@extends('layout.app')
@section('content')

<nav id="nav">
	<a href="/" class="icon fa-arrow-left active"><span>Вернуться</span></a>
</nav>

<div id="main">
	<article id="" class="panel">
		<header>
			<h2>Изменить документ</h2>
		</header>
		@include('parts._form_card')
	</article>
</div>

@endsection
