@extends('layout.auth')

@section('content')

<article id="login" class="panel">
	<header>
		<h2>Вход</h2>
	</header>
	<form method="post" action="{{ route('login') }}">
    {{ csrf_field() }}
		<div>
			<div class="row">
				<div class="12u 12u$(mobile)">
					<input type="text" name="email" placeholder="E-mail" />
					@if ($errors->has('email')) <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span> @endif
				</div>
				<div class="12u$ 12u$(mobile)">
					<input type="password" name="password" placeholder="Пароль" />
	        @if ($errors->has('password')) <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span> @endif
				</div>
				<div class="12u$">
					<input type="submit" value="Войти" />
				</div>
			</div>
		</div>
	</form>
</article>

<article id="signup" class="panel">
	<header>
		<h2>Регистрация</h2>
	</header>
	<form method="post" action="{{ route('register') }}">
    {{ csrf_field() }}
		<div>
			<div class="row">
				<div class="12u 12u$(mobile)">
					<input type="text" name="full_name" placeholder="Ваше полное имя" />
					@if ($errors->has('full_name'))<span class="help-block"><strong c#f>{{ $errors->first('full_name') }}</strong></span>@endif
				</div>
				<div class="12u 12u$(mobile)">
					<input type="text" name="name" placeholder="Логин" />
					@if ($errors->has('name'))<span class="help-block"><strong c#f>{{ $errors->first('name') }}</strong></span>@endif
				</div>
				<div class="12u 12u$(mobile)">
					<input type="text" name="email" required="" placeholder="E-mail" />
					@if ($errors->has('email'))<span class="help-block"><strong c#f>{{ $errors->first('email') }}</strong></span>@endif
				</div>
				<div class="12u 12u$(mobile)">
					<input type="password" name="password" placeholder="Пароль" />
					@if ($errors->has('password'))<span class="help-block"><strong c#f>{{ $errors->first('password') }}</strong></span>@endif
				</div>
				<div class="12u$ 12u$(mobile)">
					<input type="password" name="password_confirmation" placeholder="Повторите пароль" />
					@if ($errors->has('password_confirmation'))<span class="help-block"><strong c#f>{{ $errors->first('password_confirmation') }}</strong></span>@endif
				</div>
				<div class="12u$">
					<input type="submit" value="Зарегистрироваться" />
				</div>
			</div>
		</div>
	</form>
</article>

@endsection
