<!DOCTYPE HTML>
<html>
<head>

	<title>{{ config('app.name', 'Laravel') }}</title>

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<meta name="csrf-token" content="{{ csrf_token() }}" />

	<!--[if lte IE 8]><script src="{{ asset('assets/js/ie/html5shiv.js') }}"></script><![endif]-->
	<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}" />
	<noscript><link rel="stylesheet" href="{{ asset('assets/css/noscript.css') }}" /></noscript>
	<!--[if lte IE 8]><link rel="stylesheet" href="{{ asset('assets/css/ie8.css') }}" /><![endif]-->
</head>
<body>

	<div id="wrapper">

		<nav id="nav">
			<a href="#login" class="icon fa-sign-in active"><span>Вход</span></a>
			<a href="#signup" class="icon fa-user-plus"><span>Регистрация</span></a>
		</nav>

		<div id="main">
			@yield('content')
		</div>

		<div id="footer">
			<ul class="copyright">
				<li><p>&copy; {{ config('app.name', 'Laravel') }}. Все права защищены.</p></li>
			</ul>
		</div>
	</div>

	<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/js/skel.min.js') }}"></script>
	<script src="{{ asset('assets/js/skel-viewport.min.js') }}"></script>
	<script src="{{ asset('assets/js/util.js') }}"></script>
	<!--[if lte IE 8]><script src="{{ asset('assets/js/ie/respond.min.js') }}"></script><![endif]-->
	<script src="{{ asset('assets/js/main.js') }}"></script>

</body>
</html>
