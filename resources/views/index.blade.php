@extends('layout.app')
@section('content')

<nav id="nav">
	<a href="#home" class="icon fa-home active"><span>Главная</span></a>
	<a href="#doks" class="icon fa-folder"><span>Документы</span></a>
	<a href="#add" class="icon fa-plus"><span>Добавить</span></a>
	@if ($is_admin)
		<a href="#cats" class="icon fa-list-alt"><span>Категории</span></a>
		<a href="#users" class="icon fa-users"><span>Пользователи</span></a>
	@endif
	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
	<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="icon fa-sign-out"><span>Выход</span></a>
</nav>

<div id="main">
	<article id="home" class="panel">
		<header>
			<h1>{{ $user->full_name }}</h1>
			<p>Система для хранения ваших документов</p>
		</header>
	</article>

	<article id="doks" class="panel">
		<header>
			<h2>Мои документы</h2>
		</header>
		<table class="table">
			<thead>
				<tr>
					<td>#</td>
					<td>Название</td>
					<td>Описание</td>
					<td>Тип</td>
					<td>Действия</td>
				</tr>
			</thead>
			<tbody>
				@foreach ($doks as $item)
				<tr>
					<td>{{$item->id}}</td>
					<td>{{$item->title}}</td>
					<td>{{$item->desc}}</td>
					<td>{{$item->getCat()->title}}</td>
					<td>
						<a c#7 td:n td:u@hov href="/view/{{$item->id}}"><i class="icon fa-eye"></i></a>
						<a c#7 td:n td:u@hov href="/edit/{{$item->id}}"><i class="icon fa-edit"></i></a>
						<a c#7 td:n td:u@hov href="/delete/{{$item->id}}" onclick="return confirm('Вы уверенны?')"><i class="icon fa-trash"></i></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</article>

	<article id="add" class="panel">
		<header>
			<h2>Добавить</h2>
		</header>
		@include('parts._form_card')
	</article>

	@if ($is_admin)
		<article id="add-cat" class="panel">
			<header>
				<h2>Добавить категорию</h2>
			</header>
			@include('parts._form_cat')
		</article>
		<article id="cats" class="panel">
			<header>
				<h2>Категории</h2>
				<a href="#add-cat" class="jumplink" m:t td:n c#7 td:u@hov>Добавить</a>
			</header>
			<table class="table">
				<thead>
					<tr>
						<td>#</td>
						<td>Название</td>
						<td>Описание</td>
						<td>Действия</td>
					</tr>
				</thead>
				<tbody>
					@foreach ($cats as $item)
					<tr>
						<td>{{$item->id}}</td>
						<td>{{$item->title}}</td>
						<td>{{$item->desc}}</td>
						<td>
							<a c#7 td:n td:u@hov href="/edit-cat/{{$item->id}}"><i class="icon fa-edit"></i></a>
							<a c#7 td:n td:u@hov href="/delete-cat/{{$item->id}}" onclick="return confirm('Вы уверенны?')"><i class="icon fa-trash"></i></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</article>
		<article id="users" class="panel">
			<header>
				<h2>Категории</h2>
			</header>
			<table class="table">
				<thead>
					<tr>
						<td>#</td>
						<td>Имя</td>
						<td>Логин</td>
						<td>Почта</td>
						<td>Тип</td>
						<td>Действия</td>
					</tr>
				</thead>
				<tbody>
					@foreach ($users as $item)
					<tr>
						<td>{{$item->id}}</td>
						<td>{{$item->full_name}}</td>
						<td>{{$item->name}}</td>
						<td>{{$item->email}}</td>
						<td>{{$item->getRole()}}</td>
						<td>
							<a c#7 td:n td:u@hov href="/edit-user/{{$item->id}}"><i class="icon fa-edit"></i></a>
							<a c#7 td:n td:u@hov href="/delete-user/{{$item->id}}" onclick="return confirm('Вы уверенны?')"><i class="icon fa-trash"></i></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</article>
	@endif
</div>

@endsection
