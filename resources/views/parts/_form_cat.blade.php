{!! Form::open(array('url' => !isset($model) ? '/create-cat' : '/update-cat/' .$model->id )) !!}
<div>
	<div class="row">
		<div class="12u$">
			{!! Form::text('title', isset($model) ? $model->title : '', ['placeholder' => 'Название',]) !!}
			@if ($errors->has('title')) <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span> @endif
		</div>
		<div class="12u$">
			<textarea name="desc" placeholder="Описание" >{{isset($model) ? $model->desc : ''}}</textarea>
			@if ($errors->has('desc'))<span class="help-block"><strong>{{ $errors->first('desc') }}</strong></span>@endif
		</div>
		<div class="12u$">
			<input type="submit" value="Сохранить" />
		</div>
	</div>
</div>
{!! Form::close() !!}
