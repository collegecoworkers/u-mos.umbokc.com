{!! Form::open(array('url' => !isset($model) ? '/add' : '/update/' .$model->id )) !!}
<div>
	<div class="row">
		<div class="12u$">
			{!! Form::text('title', isset($model) ? $model->title : '', ['placeholder' => 'Название',]) !!}
			@if ($errors->has('title')) <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span> @endif
		</div>
		<div class="12u$">
			<textarea name="desc" placeholder="Описание" >{{isset($model) ? $model->desc : ''}}</textarea>
			@if ($errors->has('desc'))<span class="help-block"><strong>{{ $errors->first('desc') }}</strong></span>@endif
		</div>
		<div class="12u$">
			<label p:b>Категория</label>
			{!! Form::select('cat_id', $cats_arr, isset($model) ? $model->cat_id : '', []) !!}
			@if ($errors->has('cat_id'))<span class="help-block"><strong>{{ $errors->first('cat_id') }}</strong></span>@endif
		</div>
		<div class="12u$">
			<textarea name="props" placeholder="Реквизиты" >{{isset($model) ? $model->props : ''}}</textarea>
			@if ($errors->has('props'))<span class="help-block"><strong>{{ $errors->first('props') }}</strong></span>@endif
		</div>
		<div class="12u$">
			<input type="submit" value="Сохранить" />
		</div>
	</div>
</div>
{!! Form::close() !!}
