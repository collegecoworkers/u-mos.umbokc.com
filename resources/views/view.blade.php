@extends('layout.app')
@section('content')

<nav id="nav">
	<a href="/" class="icon fa-arrow-left active"><span>Вернуться</span></a>
</nav>

<div id="main">
	<article id="" class="panel">
		<header>
			<h2>Просмотр документа: {{ $item->title }}</h2>
		</header>
		<div>
			<div><b>Категория: </b><div p:l>{{ $cat->title }}</div></div>
			<div><b>Описание: </b><div p:l>{{ $item->desc }}</div></div>
			<div><b>Реквизиты: </b><pre p:l>{{ $item->props }}</pre></div>
		</div>
	</article>
</div>

@endsection
